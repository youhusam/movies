import React, { useState, useEffect } from "react";
import classNames from 'classnames';

export const TMDB_IMAGE_BASE_PATH = "https://image.tmdb.org/t/p/w500/";

const TMDBImage = ({ src, className, ...restProps }) => {
  const [isLoading, setIsloading] = useState(true);

  useEffect(() => {
    setIsloading(true);
  }, [src])
  return (
    <span className="tmdb-image">
      { isLoading && <span className="loader">❖</span> }
      <img src={`${TMDB_IMAGE_BASE_PATH}${src}`} className={classNames(className, { isLoading })} {...restProps} onLoad={() => { setIsloading(false) }} />
    </span>
  );
};

export default TMDBImage;
