import React, { Component, PureComponent } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import TMDBImage, { TMDB_IMAGE_BASE_PATH } from "./TMDBImage";
import './MoviesList.css'

export default class MoviesList extends PureComponent {

  static propTypes = {
    movies: PropTypes.array.isRequired,
    sortMovies: PropTypes.func.isRequired,
  }

  state = {
    selectedMovie: null,
    isSelected: false,
  }

  handleSelectMovie = item => this.setState({selectedMovie: item, isSelected: true})

  handleSortingChange = sortingType => this.props.sortMovies(sortingType)

  render() {

    const {movies} = this.props
    const {selectedMovie} = this.state

    return (
      <div className="movies-list">
        <div className="sort">
          <span>Sort by:</span>
          <SortingOptions onChange={this.handleSortingChange}/>
        </div>
        <div className="items">
          {
            movies.map(movie =>
              <MovieListItem key={movie.id} movie={movie} isSelected={selectedMovie===movie} onSelect={this.handleSelectMovie}/>
            )
          }
        </div>
          <ExpandedMovieItem movie={selectedMovie || movies[0]} opened={this.state.isSelected} clearSelected={() => this.setState({ isSelected: false })} />
      </div>
    )
  }
}

const ExpandedMovieItem = ({movie: {title, original_title, poster_path, overview, vote_average, vote_count, backdrop_path}, opened, clearSelected}) => (
  <div className={classNames("expanded-movie-item", { opened })}>
    <div className="backdrop" onClick={clearSelected}></div>
    <div className="expanded-movie-item-inner">
      <span className="close-button" onClick={clearSelected}>&times;</span>
      <span className="detail-background" style={{ backgroundImage: `url('${TMDB_IMAGE_BASE_PATH}${backdrop_path}')` }}></span>
      <div className="image-container">
        <TMDBImage src={poster_path} className="poster" />
      </div>
      <div className="description">
        <h2>{title} {title !== original_title ? `(${ original_title })` : ''}</h2>
        <div>⭐️ {vote_average} <span title="Count">({vote_count})</span></div>
        <span>{overview}</span>
      </div>
    </div>
  </div>
)

class MovieListItem extends Component {

  handleClick = () => {
    const {movie, onSelect} = this.props
    onSelect(movie)
  }

  render() {
    const {movie: {title, vote_average, poster_path}, isSelected} = this.props
    return (
      <div className={classNames('movie-list-item', { 'selected': isSelected })} onClick={this.handleClick} style={{ backgroundImage: `url('${TMDB_IMAGE_BASE_PATH}${poster_path}')`}}>
        <div className="title">
          <div className="name">{title}</div>
          <div className="rating">⭐️ {vote_average}</div>
        </div>
      </div>
    )
  }
}

class SortingOptions extends Component {

  state = {
    value: ''
  }

  handleChange = e => {
    const selectedValue = e.target.value
    const {onChange} = this.props
    this.setState({value: selectedValue})
    onChange(selectedValue)
  }

  render() {

    return (
      <select value={this.state.value} onChange={this.handleChange}>
        <option value=""></option>
        <option value="name_asc">A -> Z</option>
        <option value="name_desc">Z -> A</option>
        <option value="rating">Rating</option>
      </select>
    )
  }
}

