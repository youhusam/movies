import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {fetchTopRatedMovies, sortMovies} from '../store/actions'


import logo from './logo.svg'
import './MovieLibrary.css'
import {getMovies, getMovieLib} from '../store/selectors'
import MoviesList from './MoviesList'

class MovieLibrary extends Component {

  static propTypes = {
    movies: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired
  }

  state = {
    page: 4
  }

  timeout = null;

  componentDidMount() {
    const {fetchTopRatedMovies} = this.props
    fetchTopRatedMovies();

    window.onscroll = () => {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (
          window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight
          && !this.props.isFetching
        ) {
          fetchTopRatedMovies(this.state.page);
          this.setState({ page: this.state.page + 1 });
        }
      }, 300);
    };
  }

  render() {
    const {movies, isFetching} = this.props
    return (
      <div className="MovieLibrary">
        <header className="ML-header">
          <img src={logo} className="ML-logo" alt="logo" />
          <h1 className="ML-title">Movies</h1>
        </header>
        <div className="ML-intro">
          {
            movies.length ?
              <MoviesList movies={movies} sortMovies={this.props.sortMovies} /> :
              isFetching && <h3>Loading...</h3>
          }
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  movies: getMovies(state),
  isFetching: getMovieLib(state).isFetching
}), {fetchTopRatedMovies, sortMovies})(MovieLibrary)
