import {FETCH_MOVIES, SORT_MOVIES} from '../../actionTypes'

const initialState = {
  movies: [],
  isFetching: false,
}

export default function movies(state = initialState, action) {
  const { type, payload = [], isFetching } = action;

  switch (type) {
    case FETCH_MOVIES:
      return {
        ...state,
        movies: state.movies.concat(payload),
        isFetching,
      }

    case SORT_MOVIES:
      const sortedMovies = state.movies.sort((a, b) => {
        const order = payload.order;
        if (order === 'rating') {
          return a.vote_average > b.vote_average ? 1 : -1;
        }
        if (order === 'name_asc') {
          return a.title > b.title ? 1 : -1;
        }
        if (order === 'name_desc') {
          return a.title < b.title ? 1 : -1;
        }
        return a.id > b.id ? 1 : -1;
      })
      return {
        ...state,
        movies: [...sortedMovies]
      }

    default:
      return state
  }
}
