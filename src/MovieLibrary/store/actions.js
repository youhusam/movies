import { FETCH_MOVIES, SORT_MOVIES } from "../../actionTypes";
import topRatedMovies from "../mocks/topTatedMovies";

const URL =
  "https://api.themoviedb.org/3/movie/now_playing?api_key=54ffed57deb5a7a8688be4de3007e578";
export const loadNowPlayingPage = page =>
  window
    .fetch(`${URL}&page=${page}`)
    .then(q => q.json())
    .then(q => q.results);

export const fetchTopRatedMovies = page => async dispatch => {
  dispatch({
    type: FETCH_MOVIES,
    isFetching: true,
  });

  let movies = [];
  if (!page) {
    const values = await Promise.all([
      loadNowPlayingPage(1),
      loadNowPlayingPage(2),
      loadNowPlayingPage(3)
    ]);
    movies = []
      .concat(values[0])
      .concat(values[1])
      .concat(values[2]);
  } else {
    movies = await loadNowPlayingPage(page);
  }
  return dispatch({
    type: FETCH_MOVIES,
    payload: movies,
    isFetching: false,
  });
};

export function sortMovies(order) {
  return {
    type: SORT_MOVIES,
    payload: {
      order
    }
  };
}
